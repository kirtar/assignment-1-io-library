%define SYS_EXIT 60
%define SYS_WRITE 1
%define STDOUT 1
%define STDIN 0
%define MAX_LENGTH_8b_NUM 20
%define BASE_OF_SYSTEM 10

section .text

; Принимает код возврата и завершает текущий процесс
exit:
mov rax, SYS_EXIT
syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax,rax
	.loop:
		cmp byte[rdi+rax],0
		je .done
		inc rax
		jmp .loop
	.done:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, SYS_WRITE
	mov rdi, STDOUT
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, SYS_WRITE
	mov rdi, STDOUT
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rdi

	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, '\n'
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r8, rsp
	mov r9, BASE_OF_SYSTEM

	sub rsp, MAX_LENGTH_8b_NUM+1
	mov rdi, rsp
	add rdi, MAX_LENGTH_8b_NUM

	.loop:
		xor rdx, rdx
		div r9
		add dl, '0'
		dec rdi
		mov [rdi], dl
		test rax, rax
		jnz .loop

	call print_string
	add rsp, MAX_LENGTH_8b_NUM+1
	ret





; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	test rdi, rdi
	jge .print_abs
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	.print_abs:
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
	xor rcx, rcx

	.loop:
		mov al, [rdi + rcx]
		cmp al, [rsi + rcx]
		jne .fail

		cmp al, 0
		je .success

		inc rcx
		jmp .loop

	.success:
		mov rax, 1
		ret

	.fail:
		xor rax, rax
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	mov rax, 0
	mov rdi, STDIN
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r13
	push r14
	push r15
	xor r13,r13
	mov r14,rdi
	mov r15,rsi


	.loop:
	call read_char

	test rax,rax
	jz .break

	cmp rax, ' '
	jz .space

	cmp rax, `\t`
	jz .space

	cmp rax, `\n`
	jz .space


	cmp r13, r15
	je .fail

	mov [r14+r13], rax
	inc r13

	jmp .loop

	.break:
		mov byte[r14+r13], 0
		mov rax, r14
		mov rdx, r13
		jmp .end

	.space:
		test r13,r13
		jz .loop
		jmp .break

	.fail:
		xor rax, rax
		xor rdx, rdx
	.end:
		pop r15
		pop r14
		pop r13
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	xor rcx, rcx

	.loop:
	mov dl, byte [rdi+rcx]

	test dl,dl
	je .end

	sub dl, '0'

	cmp dl, 9
	ja .end

	cmp dl,0
	jb .end

	imul rax,rax, 10
	add rax, rdx
	inc rcx
	jmp .loop

	.end:
		test rcx, rcx
		jz .error
		mov rdx, rcx
		ret

	.error:
		xor rdx, rdx
		ret







; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, byte[rdi]
	cmp al, '+'
	je .positive
	cmp al, '-'
	je .negative
	call parse_uint
	jmp .end

	.positive:
		inc rdi
		call parse_uint
		inc rdx
		jmp .end

	.negative:
		inc rdi
		call parse_uint
		inc rdx
		test rdx,rdx
		jz .end
		neg rax

	.end:
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
xor rax, rax
.loop:
cmp rax, rdx
jge .failure
mov cl, byte[rdi + rax]
mov byte[rsi + rax], cl
inc rax
test cl, cl
je .end
jmp .loop
.failure:
xor rax, rax
.end:
ret
